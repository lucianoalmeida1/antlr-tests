
// Generated from Franca.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "FrancaListener.h"


/**
 * This class provides an empty implementation of FrancaListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  FrancaBaseListener : public FrancaListener {
public:

  virtual void enterTop_level(FrancaParser::Top_levelContext * /*ctx*/) override { }
  virtual void exitTop_level(FrancaParser::Top_levelContext * /*ctx*/) override { }

  virtual void enterFrom_decl(FrancaParser::From_declContext * /*ctx*/) override { }
  virtual void exitFrom_decl(FrancaParser::From_declContext * /*ctx*/) override { }

  virtual void enterFile_id(FrancaParser::File_idContext * /*ctx*/) override { }
  virtual void exitFile_id(FrancaParser::File_idContext * /*ctx*/) override { }

  virtual void enterImport_decl(FrancaParser::Import_declContext * /*ctx*/) override { }
  virtual void exitImport_decl(FrancaParser::Import_declContext * /*ctx*/) override { }

  virtual void enterPackage_decl(FrancaParser::Package_declContext * /*ctx*/) override { }
  virtual void exitPackage_decl(FrancaParser::Package_declContext * /*ctx*/) override { }

  virtual void enterPackage_name(FrancaParser::Package_nameContext * /*ctx*/) override { }
  virtual void exitPackage_name(FrancaParser::Package_nameContext * /*ctx*/) override { }

  virtual void enterImport_name(FrancaParser::Import_nameContext * /*ctx*/) override { }
  virtual void exitImport_name(FrancaParser::Import_nameContext * /*ctx*/) override { }

  virtual void enterTail_import_name(FrancaParser::Tail_import_nameContext * /*ctx*/) override { }
  virtual void exitTail_import_name(FrancaParser::Tail_import_nameContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

