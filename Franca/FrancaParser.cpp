
// Generated from Franca.g4 by ANTLR 4.7.1


#include "FrancaListener.h"

#include "FrancaParser.h"


using namespace antlrcpp;
using namespace antlr4;

FrancaParser::FrancaParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

FrancaParser::~FrancaParser() {
  delete _interpreter;
}

std::string FrancaParser::getGrammarFileName() const {
  return "Franca.g4";
}

const std::vector<std::string>& FrancaParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& FrancaParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- Top_levelContext ------------------------------------------------------------------

FrancaParser::Top_levelContext::Top_levelContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

FrancaParser::From_declContext* FrancaParser::Top_levelContext::from_decl() {
  return getRuleContext<FrancaParser::From_declContext>(0);
}


size_t FrancaParser::Top_levelContext::getRuleIndex() const {
  return FrancaParser::RuleTop_level;
}

void FrancaParser::Top_levelContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTop_level(this);
}

void FrancaParser::Top_levelContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTop_level(this);
}

FrancaParser::Top_levelContext* FrancaParser::top_level() {
  Top_levelContext *_localctx = _tracker.createInstance<Top_levelContext>(_ctx, getState());
  enterRule(_localctx, 0, FrancaParser::RuleTop_level);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(16);
    from_decl();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- From_declContext ------------------------------------------------------------------

FrancaParser::From_declContext::From_declContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

FrancaParser::File_idContext* FrancaParser::From_declContext::file_id() {
  return getRuleContext<FrancaParser::File_idContext>(0);
}


size_t FrancaParser::From_declContext::getRuleIndex() const {
  return FrancaParser::RuleFrom_decl;
}

void FrancaParser::From_declContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFrom_decl(this);
}

void FrancaParser::From_declContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFrom_decl(this);
}

FrancaParser::From_declContext* FrancaParser::from_decl() {
  From_declContext *_localctx = _tracker.createInstance<From_declContext>(_ctx, getState());
  enterRule(_localctx, 2, FrancaParser::RuleFrom_decl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(18);
    match(FrancaParser::T__0);
    setState(19);
    file_id();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- File_idContext ------------------------------------------------------------------

FrancaParser::File_idContext::File_idContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* FrancaParser::File_idContext::IDENTIFIER() {
  return getToken(FrancaParser::IDENTIFIER, 0);
}


size_t FrancaParser::File_idContext::getRuleIndex() const {
  return FrancaParser::RuleFile_id;
}

void FrancaParser::File_idContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFile_id(this);
}

void FrancaParser::File_idContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFile_id(this);
}

FrancaParser::File_idContext* FrancaParser::file_id() {
  File_idContext *_localctx = _tracker.createInstance<File_idContext>(_ctx, getState());
  enterRule(_localctx, 4, FrancaParser::RuleFile_id);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(27);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(21);
      match(FrancaParser::T__1);
      setState(22);
      match(FrancaParser::IDENTIFIER);
      setState(23);
      match(FrancaParser::T__2);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(24);
      match(FrancaParser::T__1);
      setState(25);
      match(FrancaParser::IDENTIFIER);
      setState(26);
      match(FrancaParser::T__1);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Import_declContext ------------------------------------------------------------------

FrancaParser::Import_declContext::Import_declContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

FrancaParser::Import_nameContext* FrancaParser::Import_declContext::import_name() {
  return getRuleContext<FrancaParser::Import_nameContext>(0);
}


size_t FrancaParser::Import_declContext::getRuleIndex() const {
  return FrancaParser::RuleImport_decl;
}

void FrancaParser::Import_declContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterImport_decl(this);
}

void FrancaParser::Import_declContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitImport_decl(this);
}

FrancaParser::Import_declContext* FrancaParser::import_decl() {
  Import_declContext *_localctx = _tracker.createInstance<Import_declContext>(_ctx, getState());
  enterRule(_localctx, 6, FrancaParser::RuleImport_decl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(29);
    match(FrancaParser::T__3);
    setState(30);
    import_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Package_declContext ------------------------------------------------------------------

FrancaParser::Package_declContext::Package_declContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

FrancaParser::Package_nameContext* FrancaParser::Package_declContext::package_name() {
  return getRuleContext<FrancaParser::Package_nameContext>(0);
}


size_t FrancaParser::Package_declContext::getRuleIndex() const {
  return FrancaParser::RulePackage_decl;
}

void FrancaParser::Package_declContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPackage_decl(this);
}

void FrancaParser::Package_declContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPackage_decl(this);
}

FrancaParser::Package_declContext* FrancaParser::package_decl() {
  Package_declContext *_localctx = _tracker.createInstance<Package_declContext>(_ctx, getState());
  enterRule(_localctx, 8, FrancaParser::RulePackage_decl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(32);
    match(FrancaParser::T__4);
    setState(33);
    package_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Package_nameContext ------------------------------------------------------------------

FrancaParser::Package_nameContext::Package_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> FrancaParser::Package_nameContext::IDENTIFIER() {
  return getTokens(FrancaParser::IDENTIFIER);
}

tree::TerminalNode* FrancaParser::Package_nameContext::IDENTIFIER(size_t i) {
  return getToken(FrancaParser::IDENTIFIER, i);
}


size_t FrancaParser::Package_nameContext::getRuleIndex() const {
  return FrancaParser::RulePackage_name;
}

void FrancaParser::Package_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPackage_name(this);
}

void FrancaParser::Package_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPackage_name(this);
}

FrancaParser::Package_nameContext* FrancaParser::package_name() {
  Package_nameContext *_localctx = _tracker.createInstance<Package_nameContext>(_ctx, getState());
  enterRule(_localctx, 10, FrancaParser::RulePackage_name);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(35);
    match(FrancaParser::IDENTIFIER);
    setState(36);
    match(FrancaParser::T__5);
    setState(40);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == FrancaParser::IDENTIFIER) {
      setState(37);
      match(FrancaParser::IDENTIFIER);
      setState(42);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Import_nameContext ------------------------------------------------------------------

FrancaParser::Import_nameContext::Import_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> FrancaParser::Import_nameContext::IDENTIFIER() {
  return getTokens(FrancaParser::IDENTIFIER);
}

tree::TerminalNode* FrancaParser::Import_nameContext::IDENTIFIER(size_t i) {
  return getToken(FrancaParser::IDENTIFIER, i);
}

FrancaParser::Tail_import_nameContext* FrancaParser::Import_nameContext::tail_import_name() {
  return getRuleContext<FrancaParser::Tail_import_nameContext>(0);
}


size_t FrancaParser::Import_nameContext::getRuleIndex() const {
  return FrancaParser::RuleImport_name;
}

void FrancaParser::Import_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterImport_name(this);
}

void FrancaParser::Import_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitImport_name(this);
}

FrancaParser::Import_nameContext* FrancaParser::import_name() {
  Import_nameContext *_localctx = _tracker.createInstance<Import_nameContext>(_ctx, getState());
  enterRule(_localctx, 12, FrancaParser::RuleImport_name);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(56);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case FrancaParser::IDENTIFIER: {
        enterOuterAlt(_localctx, 1);
        setState(44); 
        _errHandler->sync(this);
        _la = _input->LA(1);
        do {
          setState(43);
          match(FrancaParser::IDENTIFIER);
          setState(46); 
          _errHandler->sync(this);
          _la = _input->LA(1);
        } while (_la == FrancaParser::IDENTIFIER);
        setState(48);
        match(FrancaParser::T__5);
        setState(52);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == FrancaParser::IDENTIFIER) {
          setState(49);
          match(FrancaParser::IDENTIFIER);
          setState(54);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        break;
      }

      case FrancaParser::T__6: {
        enterOuterAlt(_localctx, 2);
        setState(55);
        tail_import_name();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Tail_import_nameContext ------------------------------------------------------------------

FrancaParser::Tail_import_nameContext::Tail_import_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t FrancaParser::Tail_import_nameContext::getRuleIndex() const {
  return FrancaParser::RuleTail_import_name;
}

void FrancaParser::Tail_import_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTail_import_name(this);
}

void FrancaParser::Tail_import_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<FrancaListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTail_import_name(this);
}

FrancaParser::Tail_import_nameContext* FrancaParser::tail_import_name() {
  Tail_import_nameContext *_localctx = _tracker.createInstance<Tail_import_nameContext>(_ctx, getState());
  enterRule(_localctx, 14, FrancaParser::RuleTail_import_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(58);
    match(FrancaParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> FrancaParser::_decisionToDFA;
atn::PredictionContextCache FrancaParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN FrancaParser::_atn;
std::vector<uint16_t> FrancaParser::_serializedATN;

std::vector<std::string> FrancaParser::_ruleNames = {
  "top_level", "from_decl", "file_id", "import_decl", "package_decl", "package_name", 
  "import_name", "tail_import_name"
};

std::vector<std::string> FrancaParser::_literalNames = {
  "", "'from'", "'\"'", "'.fidl\"'", "'import'", "'package'", "'.'", "'.*'"
};

std::vector<std::string> FrancaParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "IDENTIFIER", "WS"
};

dfa::Vocabulary FrancaParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> FrancaParser::_tokenNames;

FrancaParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0xb, 0x3f, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x5, 0x4, 0x1e, 0xa, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 
    0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x29, 0xa, 
    0x7, 0xc, 0x7, 0xe, 0x7, 0x2c, 0xb, 0x7, 0x3, 0x8, 0x6, 0x8, 0x2f, 0xa, 
    0x8, 0xd, 0x8, 0xe, 0x8, 0x30, 0x3, 0x8, 0x3, 0x8, 0x7, 0x8, 0x35, 0xa, 
    0x8, 0xc, 0x8, 0xe, 0x8, 0x38, 0xb, 0x8, 0x3, 0x8, 0x5, 0x8, 0x3b, 0xa, 
    0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x2, 0x2, 0xa, 0x2, 0x4, 0x6, 0x8, 
    0xa, 0xc, 0xe, 0x10, 0x2, 0x2, 0x2, 0x3b, 0x2, 0x12, 0x3, 0x2, 0x2, 
    0x2, 0x4, 0x14, 0x3, 0x2, 0x2, 0x2, 0x6, 0x1d, 0x3, 0x2, 0x2, 0x2, 0x8, 
    0x1f, 0x3, 0x2, 0x2, 0x2, 0xa, 0x22, 0x3, 0x2, 0x2, 0x2, 0xc, 0x25, 
    0x3, 0x2, 0x2, 0x2, 0xe, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x10, 0x3c, 0x3, 
    0x2, 0x2, 0x2, 0x12, 0x13, 0x5, 0x4, 0x3, 0x2, 0x13, 0x3, 0x3, 0x2, 
    0x2, 0x2, 0x14, 0x15, 0x7, 0x3, 0x2, 0x2, 0x15, 0x16, 0x5, 0x6, 0x4, 
    0x2, 0x16, 0x5, 0x3, 0x2, 0x2, 0x2, 0x17, 0x18, 0x7, 0x4, 0x2, 0x2, 
    0x18, 0x19, 0x7, 0xa, 0x2, 0x2, 0x19, 0x1e, 0x7, 0x5, 0x2, 0x2, 0x1a, 
    0x1b, 0x7, 0x4, 0x2, 0x2, 0x1b, 0x1c, 0x7, 0xa, 0x2, 0x2, 0x1c, 0x1e, 
    0x7, 0x4, 0x2, 0x2, 0x1d, 0x17, 0x3, 0x2, 0x2, 0x2, 0x1d, 0x1a, 0x3, 
    0x2, 0x2, 0x2, 0x1e, 0x7, 0x3, 0x2, 0x2, 0x2, 0x1f, 0x20, 0x7, 0x6, 
    0x2, 0x2, 0x20, 0x21, 0x5, 0xe, 0x8, 0x2, 0x21, 0x9, 0x3, 0x2, 0x2, 
    0x2, 0x22, 0x23, 0x7, 0x7, 0x2, 0x2, 0x23, 0x24, 0x5, 0xc, 0x7, 0x2, 
    0x24, 0xb, 0x3, 0x2, 0x2, 0x2, 0x25, 0x26, 0x7, 0xa, 0x2, 0x2, 0x26, 
    0x2a, 0x7, 0x8, 0x2, 0x2, 0x27, 0x29, 0x7, 0xa, 0x2, 0x2, 0x28, 0x27, 
    0x3, 0x2, 0x2, 0x2, 0x29, 0x2c, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x28, 0x3, 
    0x2, 0x2, 0x2, 0x2a, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x2b, 0xd, 0x3, 0x2, 
    0x2, 0x2, 0x2c, 0x2a, 0x3, 0x2, 0x2, 0x2, 0x2d, 0x2f, 0x7, 0xa, 0x2, 
    0x2, 0x2e, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x2f, 0x30, 0x3, 0x2, 0x2, 0x2, 
    0x30, 0x2e, 0x3, 0x2, 0x2, 0x2, 0x30, 0x31, 0x3, 0x2, 0x2, 0x2, 0x31, 
    0x32, 0x3, 0x2, 0x2, 0x2, 0x32, 0x36, 0x7, 0x8, 0x2, 0x2, 0x33, 0x35, 
    0x7, 0xa, 0x2, 0x2, 0x34, 0x33, 0x3, 0x2, 0x2, 0x2, 0x35, 0x38, 0x3, 
    0x2, 0x2, 0x2, 0x36, 0x34, 0x3, 0x2, 0x2, 0x2, 0x36, 0x37, 0x3, 0x2, 
    0x2, 0x2, 0x37, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x38, 0x36, 0x3, 0x2, 0x2, 
    0x2, 0x39, 0x3b, 0x5, 0x10, 0x9, 0x2, 0x3a, 0x2e, 0x3, 0x2, 0x2, 0x2, 
    0x3a, 0x39, 0x3, 0x2, 0x2, 0x2, 0x3b, 0xf, 0x3, 0x2, 0x2, 0x2, 0x3c, 
    0x3d, 0x7, 0x9, 0x2, 0x2, 0x3d, 0x11, 0x3, 0x2, 0x2, 0x2, 0x7, 0x1d, 
    0x2a, 0x30, 0x36, 0x3a, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

FrancaParser::Initializer FrancaParser::_init;
