
// Generated from Franca.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "FrancaParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by FrancaParser.
 */
class  FrancaListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterTop_level(FrancaParser::Top_levelContext *ctx) = 0;
  virtual void exitTop_level(FrancaParser::Top_levelContext *ctx) = 0;

  virtual void enterFrom_decl(FrancaParser::From_declContext *ctx) = 0;
  virtual void exitFrom_decl(FrancaParser::From_declContext *ctx) = 0;

  virtual void enterFile_id(FrancaParser::File_idContext *ctx) = 0;
  virtual void exitFile_id(FrancaParser::File_idContext *ctx) = 0;

  virtual void enterImport_decl(FrancaParser::Import_declContext *ctx) = 0;
  virtual void exitImport_decl(FrancaParser::Import_declContext *ctx) = 0;

  virtual void enterPackage_decl(FrancaParser::Package_declContext *ctx) = 0;
  virtual void exitPackage_decl(FrancaParser::Package_declContext *ctx) = 0;

  virtual void enterPackage_name(FrancaParser::Package_nameContext *ctx) = 0;
  virtual void exitPackage_name(FrancaParser::Package_nameContext *ctx) = 0;

  virtual void enterImport_name(FrancaParser::Import_nameContext *ctx) = 0;
  virtual void exitImport_name(FrancaParser::Import_nameContext *ctx) = 0;

  virtual void enterTail_import_name(FrancaParser::Tail_import_nameContext *ctx) = 0;
  virtual void exitTail_import_name(FrancaParser::Tail_import_nameContext *ctx) = 0;


};

