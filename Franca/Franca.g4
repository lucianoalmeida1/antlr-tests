grammar Franca;

top_level: 
		from_decl
        | import_decl
        | package_decl
	;

IDENTIFIER
    :   [A-Za-z0-9]+
    ;
from_decl : 'from' file_id ;
file_id: '"'IDENTIFIER'.fidl"' | '"'IDENTIFIER'"';
import_decl  : 'import' import_name;
package_decl : 'package' package_name;
package_name : IDENTIFIER'.'IDENTIFIER*;
import_name  : IDENTIFIER+'.'IDENTIFIER* | tail_import_name;
tail_import_name: '.*';



WS : [ \t\r\n]+ -> skip ; 