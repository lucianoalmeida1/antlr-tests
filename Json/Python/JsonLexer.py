# Generated from ../Json.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\17")
        buf.write("`\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\6\b\63\n\b\r\b\16")
        buf.write("\b\64\3\b\3\b\3\t\3\t\6\t;\n\t\r\t\16\t<\3\t\3\t\3\n\3")
        buf.write("\n\3\n\3\n\3\13\6\13F\n\13\r\13\16\13G\3\f\6\fK\n\f\r")
        buf.write("\f\16\fL\3\f\3\f\6\fQ\n\f\r\f\16\fR\3\r\6\rV\n\r\r\r\16")
        buf.write("\rW\3\16\6\16[\n\16\r\16\16\16\\\3\16\3\16\2\2\17\3\3")
        buf.write("\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\3\2\b\3\2))\3\2$$\3\2\62;\4\2..\60\60\5\2\62;C")
        buf.write("\\c|\5\2\13\f\17\17\"\"\2f\2\3\3\2\2\2\2\5\3\2\2\2\2\7")
        buf.write("\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2")
        buf.write("\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2")
        buf.write("\2\2\31\3\2\2\2\2\33\3\2\2\2\3\35\3\2\2\2\5\37\3\2\2\2")
        buf.write("\7!\3\2\2\2\t#\3\2\2\2\13%\3\2\2\2\r*\3\2\2\2\17\60\3")
        buf.write("\2\2\2\218\3\2\2\2\23@\3\2\2\2\25E\3\2\2\2\27J\3\2\2\2")
        buf.write("\31U\3\2\2\2\33Z\3\2\2\2\35\36\7}\2\2\36\4\3\2\2\2\37")
        buf.write(" \7.\2\2 \6\3\2\2\2!\"\7\177\2\2\"\b\3\2\2\2#$\7<\2\2")
        buf.write("$\n\3\2\2\2%&\7v\2\2&\'\7t\2\2\'(\7w\2\2()\7g\2\2)\f\3")
        buf.write("\2\2\2*+\7h\2\2+,\7c\2\2,-\7n\2\2-.\7u\2\2./\7g\2\2/\16")
        buf.write("\3\2\2\2\60\62\7)\2\2\61\63\n\2\2\2\62\61\3\2\2\2\63\64")
        buf.write("\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\66\3\2\2\2\66")
        buf.write("\67\7)\2\2\67\20\3\2\2\28:\7$\2\29;\n\3\2\2:9\3\2\2\2")
        buf.write(";<\3\2\2\2<:\3\2\2\2<=\3\2\2\2=>\3\2\2\2>?\7$\2\2?\22")
        buf.write("\3\2\2\2@A\7$\2\2AB\5\31\r\2BC\7$\2\2C\24\3\2\2\2DF\t")
        buf.write("\4\2\2ED\3\2\2\2FG\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\26\3\2")
        buf.write("\2\2IK\t\4\2\2JI\3\2\2\2KL\3\2\2\2LJ\3\2\2\2LM\3\2\2\2")
        buf.write("MN\3\2\2\2NP\t\5\2\2OQ\t\4\2\2PO\3\2\2\2QR\3\2\2\2RP\3")
        buf.write("\2\2\2RS\3\2\2\2S\30\3\2\2\2TV\t\6\2\2UT\3\2\2\2VW\3\2")
        buf.write("\2\2WU\3\2\2\2WX\3\2\2\2X\32\3\2\2\2Y[\t\7\2\2ZY\3\2\2")
        buf.write("\2[\\\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]^\3\2\2\2^_\b\16\2")
        buf.write("\2_\34\3\2\2\2\n\2\64<GLRW\\\3\b\2\2")
        return buf.getvalue()


class JsonLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    SINGLE_STRING = 7
    DOUBLE_STRING = 8
    KEY_NAME = 9
    NUMBER_DECIMAL = 10
    NUMBER_FLOAT = 11
    IDENTIFIER = 12
    WS = 13

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'{'", "','", "'}'", "':'", "'true'", "'false'" ]

    symbolicNames = [ "<INVALID>",
            "SINGLE_STRING", "DOUBLE_STRING", "KEY_NAME", "NUMBER_DECIMAL", 
            "NUMBER_FLOAT", "IDENTIFIER", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "SINGLE_STRING", 
                  "DOUBLE_STRING", "KEY_NAME", "NUMBER_DECIMAL", "NUMBER_FLOAT", 
                  "IDENTIFIER", "WS" ]

    grammarFileName = "Json.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


