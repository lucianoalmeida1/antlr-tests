# Generated from ../Json.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .JsonParser import JsonParser
else:
    from JsonParser import JsonParser

# This class defines a complete listener for a parse tree produced by JsonParser.
class JsonListener(ParseTreeListener):

    # Enter a parse tree produced by JsonParser#json.
    def enterJson(self, ctx:JsonParser.JsonContext):
        pass

    # Exit a parse tree produced by JsonParser#json.
    def exitJson(self, ctx:JsonParser.JsonContext):
        pass


    # Enter a parse tree produced by JsonParser#jsonObject.
    def enterJsonObject(self, ctx:JsonParser.JsonObjectContext):
        pass

    # Exit a parse tree produced by JsonParser#jsonObject.
    def exitJsonObject(self, ctx:JsonParser.JsonObjectContext):
        pass


    # Enter a parse tree produced by JsonParser#keyValuePair.
    def enterKeyValuePair(self, ctx:JsonParser.KeyValuePairContext):
        pass

    # Exit a parse tree produced by JsonParser#keyValuePair.
    def exitKeyValuePair(self, ctx:JsonParser.KeyValuePairContext):
        pass


    # Enter a parse tree produced by JsonParser#primitive.
    def enterPrimitive(self, ctx:JsonParser.PrimitiveContext):
        pass

    # Exit a parse tree produced by JsonParser#primitive.
    def exitPrimitive(self, ctx:JsonParser.PrimitiveContext):
        pass


    # Enter a parse tree produced by JsonParser#string.
    def enterString(self, ctx:JsonParser.StringContext):
        pass

    # Exit a parse tree produced by JsonParser#string.
    def exitString(self, ctx:JsonParser.StringContext):
        pass


    # Enter a parse tree produced by JsonParser#number.
    def enterNumber(self, ctx:JsonParser.NumberContext):
        pass

    # Exit a parse tree produced by JsonParser#number.
    def exitNumber(self, ctx:JsonParser.NumberContext):
        pass


    # Enter a parse tree produced by JsonParser#bool_type.
    def enterBool_type(self, ctx:JsonParser.Bool_typeContext):
        pass

    # Exit a parse tree produced by JsonParser#bool_type.
    def exitBool_type(self, ctx:JsonParser.Bool_typeContext):
        pass


