# Generated from ../Json.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\17")
        buf.write("\67\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4")
        buf.write("\b\t\b\3\2\7\2\22\n\2\f\2\16\2\25\13\2\3\2\3\2\3\3\3\3")
        buf.write("\3\3\3\3\7\3\35\n\3\f\3\16\3 \13\3\5\3\"\n\3\3\3\3\3\3")
        buf.write("\4\3\4\3\4\3\4\5\4*\n\4\3\5\3\5\3\5\5\5/\n\5\3\6\3\6\3")
        buf.write("\7\3\7\3\b\3\b\3\b\2\2\t\2\4\6\b\n\f\16\2\5\3\2\t\n\3")
        buf.write("\2\f\r\3\2\7\b\2\65\2\23\3\2\2\2\4\30\3\2\2\2\6%\3\2\2")
        buf.write("\2\b.\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2\16\64\3\2\2\2")
        buf.write("\20\22\5\4\3\2\21\20\3\2\2\2\22\25\3\2\2\2\23\21\3\2\2")
        buf.write("\2\23\24\3\2\2\2\24\26\3\2\2\2\25\23\3\2\2\2\26\27\7\2")
        buf.write("\2\3\27\3\3\2\2\2\30!\7\3\2\2\31\36\5\6\4\2\32\33\7\4")
        buf.write("\2\2\33\35\5\6\4\2\34\32\3\2\2\2\35 \3\2\2\2\36\34\3\2")
        buf.write("\2\2\36\37\3\2\2\2\37\"\3\2\2\2 \36\3\2\2\2!\31\3\2\2")
        buf.write("\2!\"\3\2\2\2\"#\3\2\2\2#$\7\5\2\2$\5\3\2\2\2%&\7\n\2")
        buf.write("\2&)\7\6\2\2\'*\5\b\5\2(*\5\4\3\2)\'\3\2\2\2)(\3\2\2\2")
        buf.write("*\7\3\2\2\2+/\5\n\6\2,/\5\16\b\2-/\5\f\7\2.+\3\2\2\2.")
        buf.write(",\3\2\2\2.-\3\2\2\2/\t\3\2\2\2\60\61\t\2\2\2\61\13\3\2")
        buf.write("\2\2\62\63\t\3\2\2\63\r\3\2\2\2\64\65\t\4\2\2\65\17\3")
        buf.write("\2\2\2\7\23\36!).")
        return buf.getvalue()


class JsonParser ( Parser ):

    grammarFileName = "Json.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "','", "'}'", "':'", "'true'", 
                     "'false'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "SINGLE_STRING", 
                      "DOUBLE_STRING", "KEY_NAME", "NUMBER_DECIMAL", "NUMBER_FLOAT", 
                      "IDENTIFIER", "WS" ]

    RULE_json = 0
    RULE_jsonObject = 1
    RULE_keyValuePair = 2
    RULE_primitive = 3
    RULE_string = 4
    RULE_number = 5
    RULE_bool_type = 6

    ruleNames =  [ "json", "jsonObject", "keyValuePair", "primitive", "string", 
                   "number", "bool_type" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    SINGLE_STRING=7
    DOUBLE_STRING=8
    KEY_NAME=9
    NUMBER_DECIMAL=10
    NUMBER_FLOAT=11
    IDENTIFIER=12
    WS=13

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class JsonContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(JsonParser.EOF, 0)

        def jsonObject(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(JsonParser.JsonObjectContext)
            else:
                return self.getTypedRuleContext(JsonParser.JsonObjectContext,i)


        def getRuleIndex(self):
            return JsonParser.RULE_json

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterJson" ):
                listener.enterJson(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitJson" ):
                listener.exitJson(self)




    def json(self):

        localctx = JsonParser.JsonContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_json)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==JsonParser.T__0:
                self.state = 14
                self.jsonObject()
                self.state = 19
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 20
            self.match(JsonParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class JsonObjectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def keyValuePair(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(JsonParser.KeyValuePairContext)
            else:
                return self.getTypedRuleContext(JsonParser.KeyValuePairContext,i)


        def getRuleIndex(self):
            return JsonParser.RULE_jsonObject

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterJsonObject" ):
                listener.enterJsonObject(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitJsonObject" ):
                listener.exitJsonObject(self)




    def jsonObject(self):

        localctx = JsonParser.JsonObjectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_jsonObject)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 22
            self.match(JsonParser.T__0)
            self.state = 31
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==JsonParser.DOUBLE_STRING:
                self.state = 23
                self.keyValuePair()
                self.state = 28
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==JsonParser.T__1:
                    self.state = 24
                    self.match(JsonParser.T__1)
                    self.state = 25
                    self.keyValuePair()
                    self.state = 30
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 33
            self.match(JsonParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class KeyValuePairContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DOUBLE_STRING(self):
            return self.getToken(JsonParser.DOUBLE_STRING, 0)

        def primitive(self):
            return self.getTypedRuleContext(JsonParser.PrimitiveContext,0)


        def jsonObject(self):
            return self.getTypedRuleContext(JsonParser.JsonObjectContext,0)


        def getRuleIndex(self):
            return JsonParser.RULE_keyValuePair

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterKeyValuePair" ):
                listener.enterKeyValuePair(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitKeyValuePair" ):
                listener.exitKeyValuePair(self)




    def keyValuePair(self):

        localctx = JsonParser.KeyValuePairContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_keyValuePair)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 35
            self.match(JsonParser.DOUBLE_STRING)
            self.state = 36
            self.match(JsonParser.T__3)
            self.state = 39
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [JsonParser.T__4, JsonParser.T__5, JsonParser.SINGLE_STRING, JsonParser.DOUBLE_STRING, JsonParser.NUMBER_DECIMAL, JsonParser.NUMBER_FLOAT]:
                self.state = 37
                self.primitive()
                pass
            elif token in [JsonParser.T__0]:
                self.state = 38
                self.jsonObject()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PrimitiveContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def string(self):
            return self.getTypedRuleContext(JsonParser.StringContext,0)


        def bool_type(self):
            return self.getTypedRuleContext(JsonParser.Bool_typeContext,0)


        def number(self):
            return self.getTypedRuleContext(JsonParser.NumberContext,0)


        def getRuleIndex(self):
            return JsonParser.RULE_primitive

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitive" ):
                listener.enterPrimitive(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitive" ):
                listener.exitPrimitive(self)




    def primitive(self):

        localctx = JsonParser.PrimitiveContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_primitive)
        try:
            self.state = 44
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [JsonParser.SINGLE_STRING, JsonParser.DOUBLE_STRING]:
                self.enterOuterAlt(localctx, 1)
                self.state = 41
                self.string()
                pass
            elif token in [JsonParser.T__4, JsonParser.T__5]:
                self.enterOuterAlt(localctx, 2)
                self.state = 42
                self.bool_type()
                pass
            elif token in [JsonParser.NUMBER_DECIMAL, JsonParser.NUMBER_FLOAT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 43
                self.number()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SINGLE_STRING(self):
            return self.getToken(JsonParser.SINGLE_STRING, 0)

        def DOUBLE_STRING(self):
            return self.getToken(JsonParser.DOUBLE_STRING, 0)

        def getRuleIndex(self):
            return JsonParser.RULE_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)




    def string(self):

        localctx = JsonParser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_string)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            _la = self._input.LA(1)
            if not(_la==JsonParser.SINGLE_STRING or _la==JsonParser.DOUBLE_STRING):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER_DECIMAL(self):
            return self.getToken(JsonParser.NUMBER_DECIMAL, 0)

        def NUMBER_FLOAT(self):
            return self.getToken(JsonParser.NUMBER_FLOAT, 0)

        def getRuleIndex(self):
            return JsonParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)




    def number(self):

        localctx = JsonParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_number)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            _la = self._input.LA(1)
            if not(_la==JsonParser.NUMBER_DECIMAL or _la==JsonParser.NUMBER_FLOAT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Bool_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return JsonParser.RULE_bool_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBool_type" ):
                listener.enterBool_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBool_type" ):
                listener.exitBool_type(self)




    def bool_type(self):

        localctx = JsonParser.Bool_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_bool_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 50
            _la = self._input.LA(1)
            if not(_la==JsonParser.T__4 or _la==JsonParser.T__5):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





