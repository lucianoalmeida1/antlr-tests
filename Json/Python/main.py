import sys
from antlr4 import *
from JsonLexer import JsonLexer
from JsonParser import JsonParser
from JsonListener import JsonListener
 
class JsonCustom(JsonListener):
    # Enter a parse tree produced by JsonParser#json.
    def enterJson(self, ctx:JsonParser.JsonContext):
        pass

    # Exit a parse tree produced by JsonParser#json.
    def exitJson(self, ctx:JsonParser.JsonContext):
        pass


    # Enter a parse tree produced by JsonParser#jsonObject.
    def enterJsonObject(self, ctx:JsonParser.JsonObjectContext):
        pass

    # Exit a parse tree produced by JsonParser#jsonObject.
    def exitJsonObject(self, ctx:JsonParser.JsonObjectContext):
        pass

    # Enter a parse tree produced by JsonParser#keyValuePair.
    def enterKeyValuePair(self, ctx:JsonParser.KeyValuePairContext):
        print('enterKeyValuePair: ' + ctx.getText())

    # Exit a parse tree produced by JsonParser#keyValuePair.
    def exitKeyValuePair(self, ctx:JsonParser.KeyValuePairContext):
        pass

    # Enter a parse tree produced by JsonParser#primitive.
    def enterPrimitive(self, ctx:JsonParser.PrimitiveContext):
        pass

    # Exit a parse tree produced by JsonParser#primitive.
    def exitPrimitive(self, ctx:JsonParser.PrimitiveContext):
        pass


    # Enter a parse tree produced by JsonParser#string.
    def enterString(self, ctx:JsonParser.StringContext):
        pass

    # Exit a parse tree produced by JsonParser#string.
    def exitString(self, ctx:JsonParser.StringContext):
        pass

    # Enter a parse tree produced by JsonParser#bool_type.
    def enterBool_type(self, ctx:JsonParser.Bool_typeContext):
        print('enterBool_type: ' + ctx.getText())

    # Exit a parse tree produced by JsonParser#bool_type.
    def exitBool_type(self, ctx:JsonParser.Bool_typeContext):
        pass

def main(argv):
    input_stream = FileStream(argv[1])
    lexer = JsonLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = JsonParser(stream)
    tree = parser.json()
    listener = JsonCustom()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    
 
if __name__ == '__main__':
    main(sys.argv)
