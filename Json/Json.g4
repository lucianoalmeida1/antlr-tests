grammar Json;

json
    : jsonObject* EOF | jsonArray* EOF
    ;

jsonObject
    : '{' (keyValuePair (',' keyValuePair)*)? '}'
    ;
jsonArray
    : '[' (jsonObject (',' jsonObject)*)? ']'
    ;
keyValuePair
    : string ':' (primitive | jsonObject)
    ;

primitive
    : string
    | bool_type
    | number
    | null_type
    ;

string
    : SINGLE_STRING
    | DOUBLE_STRING
    ;

number
    : NUMBER_DECIMAL | NUMBER_FLOAT
    ;

SINGLE_STRING
    : '\'' ~('\'')+ '\''
    ;

DOUBLE_STRING
    : '"' ~('"')+ '"'
    ;
null_type:
    'null'
    ;

NUMBER_DECIMAL
    : [0-9]+
    ;

NUMBER_FLOAT
    : [0-9]+('.'|',')[0-9]+
    ;

bool_type
    : 'true'
    | 'false'
    ;

IDENTIFIER
    :   [A-Za-z0-9]+
    ;

WS
    :   [ \t\r\n]+ -> skip
    ;