//
//  main.cpp
//  AntlrJSON
//
//  Created by Almeida Luciano, (Luciano.Almeida@partner.bmw.com.br) on 5/17/19.
//  Copyright © 2019 Almeida Luciano, (Luciano.Almeida@partner.bmw.com.br). All rights reserved.
//

#include <iostream>
#include "antlr4-runtime.h"
#include "Parser/JsonBaseListener.h"
#include "Parser/JsonLexer.h"
#include "Parser/JsonParser.h"

class CustomListener: public JsonBaseListener {
public:
    void enterKeyValuePair(JsonParser::KeyValuePairContext * ctx) override {
        std::cout << ctx->getText() << std::endl;
    }
    
    void enterJson(JsonParser::JsonContext * ctx) override {
        std::cout << ctx->getText() << std::endl;
    }
    
    void enterJsonObject(JsonParser::JsonObjectContext * ctx) override {
        std::cout << ctx->getText() << std::endl;
    }
    
    void enterString(JsonParser::StringContext * ctx) override {
        std::cout << "string -> " << ctx->getText() << std::endl;
    }
    
    void enterNumber(JsonParser::NumberContext * ctx) override {
        std::cout << "number -> "<< ctx->getText() << std::endl;
    }
    
    void enterBool_type(JsonParser::Bool_typeContext * ctx) override {
        std::cout << "bool -> "<< ctx->getText() << std::endl;
    }
    void enterNull_type(JsonParser::Null_typeContext * /*ctx*/) override {
        std::cout << "null" << std::endl;

    }
    void enterJsonArray(JsonParser::JsonArrayContext *ctx) override {
        std::cout << "json_array" << std::endl;
    }
};

void print(std::string str, int times) {
    for (int i = 0; i < times; i++) {
        std::cout << str;
    }
    std::cout << std::endl;
}

void parseJSONFile(std::ifstream& stream) {
    antlr4::ANTLRInputStream input(stream);
    JsonLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    
    JsonParser parser(&tokens);
    
    auto txt = parser.json()->toStringTree();
    std::cout << "Text: " << txt << std::endl;
    parser.reset();
    
    std::vector<std::string> toks;
    
    print("=", 30);
    auto size = parser.getTokenStream()->size();
    std::cout << "Tokens size: " << size << std::endl;
    
    for (int i = 0; i <size ; i++) {
        std::cout << "Tok: " << parser.getTokenStream()->get(i)->getText() << std::endl;
    }
    
    print("=", 30);
    
    CustomListener listener;
    auto tree = parser.json();
    antlr4::tree::ParseTreeWalker w;
    w.walk(&listener, tree);
}

int main(int argc, const char * argv[]) {
    for (int i = 1; i < argc; i++) {
        std::cout << argv[i];
        print("=", 20);
        std::ifstream stream;
        stream.open(argv[i]);
        parseJSONFile(stream);
    }
    return 0;
}
