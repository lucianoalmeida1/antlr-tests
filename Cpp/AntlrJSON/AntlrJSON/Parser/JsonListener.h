
// Generated from Json.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "JsonParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by JsonParser.
 */
class  JsonListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterJson(JsonParser::JsonContext *ctx) = 0;
  virtual void exitJson(JsonParser::JsonContext *ctx) = 0;

  virtual void enterJsonObject(JsonParser::JsonObjectContext *ctx) = 0;
  virtual void exitJsonObject(JsonParser::JsonObjectContext *ctx) = 0;

  virtual void enterJsonArray(JsonParser::JsonArrayContext *ctx) = 0;
  virtual void exitJsonArray(JsonParser::JsonArrayContext *ctx) = 0;

  virtual void enterKeyValuePair(JsonParser::KeyValuePairContext *ctx) = 0;
  virtual void exitKeyValuePair(JsonParser::KeyValuePairContext *ctx) = 0;

  virtual void enterPrimitive(JsonParser::PrimitiveContext *ctx) = 0;
  virtual void exitPrimitive(JsonParser::PrimitiveContext *ctx) = 0;

  virtual void enterString(JsonParser::StringContext *ctx) = 0;
  virtual void exitString(JsonParser::StringContext *ctx) = 0;

  virtual void enterNumber(JsonParser::NumberContext *ctx) = 0;
  virtual void exitNumber(JsonParser::NumberContext *ctx) = 0;

  virtual void enterNull_type(JsonParser::Null_typeContext *ctx) = 0;
  virtual void exitNull_type(JsonParser::Null_typeContext *ctx) = 0;

  virtual void enterBool_type(JsonParser::Bool_typeContext *ctx) = 0;
  virtual void exitBool_type(JsonParser::Bool_typeContext *ctx) = 0;


};

