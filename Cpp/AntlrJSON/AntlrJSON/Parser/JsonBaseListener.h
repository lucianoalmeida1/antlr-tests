
// Generated from Json.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "JsonListener.h"


/**
 * This class provides an empty implementation of JsonListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  JsonBaseListener : public JsonListener {
public:

  virtual void enterJson(JsonParser::JsonContext * /*ctx*/) override { }
  virtual void exitJson(JsonParser::JsonContext * /*ctx*/) override { }

  virtual void enterJsonObject(JsonParser::JsonObjectContext * /*ctx*/) override { }
  virtual void exitJsonObject(JsonParser::JsonObjectContext * /*ctx*/) override { }

  virtual void enterJsonArray(JsonParser::JsonArrayContext * /*ctx*/) override { }
  virtual void exitJsonArray(JsonParser::JsonArrayContext * /*ctx*/) override { }

  virtual void enterKeyValuePair(JsonParser::KeyValuePairContext * /*ctx*/) override { }
  virtual void exitKeyValuePair(JsonParser::KeyValuePairContext * /*ctx*/) override { }

  virtual void enterPrimitive(JsonParser::PrimitiveContext * /*ctx*/) override { }
  virtual void exitPrimitive(JsonParser::PrimitiveContext * /*ctx*/) override { }

  virtual void enterString(JsonParser::StringContext * /*ctx*/) override { }
  virtual void exitString(JsonParser::StringContext * /*ctx*/) override { }

  virtual void enterNumber(JsonParser::NumberContext * /*ctx*/) override { }
  virtual void exitNumber(JsonParser::NumberContext * /*ctx*/) override { }

  virtual void enterNull_type(JsonParser::Null_typeContext * /*ctx*/) override { }
  virtual void exitNull_type(JsonParser::Null_typeContext * /*ctx*/) override { }

  virtual void enterBool_type(JsonParser::Bool_typeContext * /*ctx*/) override { }
  virtual void exitBool_type(JsonParser::Bool_typeContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

