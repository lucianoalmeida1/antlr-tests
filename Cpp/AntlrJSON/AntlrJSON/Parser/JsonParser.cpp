
// Generated from Json.g4 by ANTLR 4.7.2


#include "JsonListener.h"

#include "JsonParser.h"


using namespace antlrcpp;
using namespace antlr4;

JsonParser::JsonParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

JsonParser::~JsonParser() {
  delete _interpreter;
}

std::string JsonParser::getGrammarFileName() const {
  return "Json.g4";
}

const std::vector<std::string>& JsonParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& JsonParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- JsonContext ------------------------------------------------------------------

JsonParser::JsonContext::JsonContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* JsonParser::JsonContext::EOF() {
  return getToken(JsonParser::EOF, 0);
}

std::vector<JsonParser::JsonObjectContext *> JsonParser::JsonContext::jsonObject() {
  return getRuleContexts<JsonParser::JsonObjectContext>();
}

JsonParser::JsonObjectContext* JsonParser::JsonContext::jsonObject(size_t i) {
  return getRuleContext<JsonParser::JsonObjectContext>(i);
}

std::vector<JsonParser::JsonArrayContext *> JsonParser::JsonContext::jsonArray() {
  return getRuleContexts<JsonParser::JsonArrayContext>();
}

JsonParser::JsonArrayContext* JsonParser::JsonContext::jsonArray(size_t i) {
  return getRuleContext<JsonParser::JsonArrayContext>(i);
}


size_t JsonParser::JsonContext::getRuleIndex() const {
  return JsonParser::RuleJson;
}

void JsonParser::JsonContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterJson(this);
}

void JsonParser::JsonContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitJson(this);
}

JsonParser::JsonContext* JsonParser::json() {
  JsonContext *_localctx = _tracker.createInstance<JsonContext>(_ctx, getState());
  enterRule(_localctx, 0, JsonParser::RuleJson);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(32);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(21);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == JsonParser::T__0) {
        setState(18);
        jsonObject();
        setState(23);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(24);
      match(JsonParser::EOF);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(28);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == JsonParser::T__3) {
        setState(25);
        jsonArray();
        setState(30);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(31);
      match(JsonParser::EOF);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- JsonObjectContext ------------------------------------------------------------------

JsonParser::JsonObjectContext::JsonObjectContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<JsonParser::KeyValuePairContext *> JsonParser::JsonObjectContext::keyValuePair() {
  return getRuleContexts<JsonParser::KeyValuePairContext>();
}

JsonParser::KeyValuePairContext* JsonParser::JsonObjectContext::keyValuePair(size_t i) {
  return getRuleContext<JsonParser::KeyValuePairContext>(i);
}


size_t JsonParser::JsonObjectContext::getRuleIndex() const {
  return JsonParser::RuleJsonObject;
}

void JsonParser::JsonObjectContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterJsonObject(this);
}

void JsonParser::JsonObjectContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitJsonObject(this);
}

JsonParser::JsonObjectContext* JsonParser::jsonObject() {
  JsonObjectContext *_localctx = _tracker.createInstance<JsonObjectContext>(_ctx, getState());
  enterRule(_localctx, 2, JsonParser::RuleJsonObject);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(34);
    match(JsonParser::T__0);
    setState(43);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == JsonParser::SINGLE_STRING

    || _la == JsonParser::DOUBLE_STRING) {
      setState(35);
      keyValuePair();
      setState(40);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == JsonParser::T__1) {
        setState(36);
        match(JsonParser::T__1);
        setState(37);
        keyValuePair();
        setState(42);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
    }
    setState(45);
    match(JsonParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- JsonArrayContext ------------------------------------------------------------------

JsonParser::JsonArrayContext::JsonArrayContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<JsonParser::JsonObjectContext *> JsonParser::JsonArrayContext::jsonObject() {
  return getRuleContexts<JsonParser::JsonObjectContext>();
}

JsonParser::JsonObjectContext* JsonParser::JsonArrayContext::jsonObject(size_t i) {
  return getRuleContext<JsonParser::JsonObjectContext>(i);
}


size_t JsonParser::JsonArrayContext::getRuleIndex() const {
  return JsonParser::RuleJsonArray;
}

void JsonParser::JsonArrayContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterJsonArray(this);
}

void JsonParser::JsonArrayContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitJsonArray(this);
}

JsonParser::JsonArrayContext* JsonParser::jsonArray() {
  JsonArrayContext *_localctx = _tracker.createInstance<JsonArrayContext>(_ctx, getState());
  enterRule(_localctx, 4, JsonParser::RuleJsonArray);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(47);
    match(JsonParser::T__3);
    setState(56);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == JsonParser::T__0) {
      setState(48);
      jsonObject();
      setState(53);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == JsonParser::T__1) {
        setState(49);
        match(JsonParser::T__1);
        setState(50);
        jsonObject();
        setState(55);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
    }
    setState(58);
    match(JsonParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- KeyValuePairContext ------------------------------------------------------------------

JsonParser::KeyValuePairContext::KeyValuePairContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

JsonParser::StringContext* JsonParser::KeyValuePairContext::string() {
  return getRuleContext<JsonParser::StringContext>(0);
}

JsonParser::PrimitiveContext* JsonParser::KeyValuePairContext::primitive() {
  return getRuleContext<JsonParser::PrimitiveContext>(0);
}

JsonParser::JsonObjectContext* JsonParser::KeyValuePairContext::jsonObject() {
  return getRuleContext<JsonParser::JsonObjectContext>(0);
}


size_t JsonParser::KeyValuePairContext::getRuleIndex() const {
  return JsonParser::RuleKeyValuePair;
}

void JsonParser::KeyValuePairContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterKeyValuePair(this);
}

void JsonParser::KeyValuePairContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitKeyValuePair(this);
}

JsonParser::KeyValuePairContext* JsonParser::keyValuePair() {
  KeyValuePairContext *_localctx = _tracker.createInstance<KeyValuePairContext>(_ctx, getState());
  enterRule(_localctx, 6, JsonParser::RuleKeyValuePair);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(60);
    string();
    setState(61);
    match(JsonParser::T__5);
    setState(64);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case JsonParser::T__6:
      case JsonParser::T__7:
      case JsonParser::T__8:
      case JsonParser::SINGLE_STRING:
      case JsonParser::DOUBLE_STRING:
      case JsonParser::NUMBER_DECIMAL:
      case JsonParser::NUMBER_FLOAT: {
        setState(62);
        primitive();
        break;
      }

      case JsonParser::T__0: {
        setState(63);
        jsonObject();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PrimitiveContext ------------------------------------------------------------------

JsonParser::PrimitiveContext::PrimitiveContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

JsonParser::StringContext* JsonParser::PrimitiveContext::string() {
  return getRuleContext<JsonParser::StringContext>(0);
}

JsonParser::Bool_typeContext* JsonParser::PrimitiveContext::bool_type() {
  return getRuleContext<JsonParser::Bool_typeContext>(0);
}

JsonParser::NumberContext* JsonParser::PrimitiveContext::number() {
  return getRuleContext<JsonParser::NumberContext>(0);
}

JsonParser::Null_typeContext* JsonParser::PrimitiveContext::null_type() {
  return getRuleContext<JsonParser::Null_typeContext>(0);
}


size_t JsonParser::PrimitiveContext::getRuleIndex() const {
  return JsonParser::RulePrimitive;
}

void JsonParser::PrimitiveContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrimitive(this);
}

void JsonParser::PrimitiveContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrimitive(this);
}

JsonParser::PrimitiveContext* JsonParser::primitive() {
  PrimitiveContext *_localctx = _tracker.createInstance<PrimitiveContext>(_ctx, getState());
  enterRule(_localctx, 8, JsonParser::RulePrimitive);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(70);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case JsonParser::SINGLE_STRING:
      case JsonParser::DOUBLE_STRING: {
        enterOuterAlt(_localctx, 1);
        setState(66);
        string();
        break;
      }

      case JsonParser::T__7:
      case JsonParser::T__8: {
        enterOuterAlt(_localctx, 2);
        setState(67);
        bool_type();
        break;
      }

      case JsonParser::NUMBER_DECIMAL:
      case JsonParser::NUMBER_FLOAT: {
        enterOuterAlt(_localctx, 3);
        setState(68);
        number();
        break;
      }

      case JsonParser::T__6: {
        enterOuterAlt(_localctx, 4);
        setState(69);
        null_type();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringContext ------------------------------------------------------------------

JsonParser::StringContext::StringContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* JsonParser::StringContext::SINGLE_STRING() {
  return getToken(JsonParser::SINGLE_STRING, 0);
}

tree::TerminalNode* JsonParser::StringContext::DOUBLE_STRING() {
  return getToken(JsonParser::DOUBLE_STRING, 0);
}


size_t JsonParser::StringContext::getRuleIndex() const {
  return JsonParser::RuleString;
}

void JsonParser::StringContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterString(this);
}

void JsonParser::StringContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitString(this);
}

JsonParser::StringContext* JsonParser::string() {
  StringContext *_localctx = _tracker.createInstance<StringContext>(_ctx, getState());
  enterRule(_localctx, 10, JsonParser::RuleString);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(72);
    _la = _input->LA(1);
    if (!(_la == JsonParser::SINGLE_STRING

    || _la == JsonParser::DOUBLE_STRING)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

JsonParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* JsonParser::NumberContext::NUMBER_DECIMAL() {
  return getToken(JsonParser::NUMBER_DECIMAL, 0);
}

tree::TerminalNode* JsonParser::NumberContext::NUMBER_FLOAT() {
  return getToken(JsonParser::NUMBER_FLOAT, 0);
}


size_t JsonParser::NumberContext::getRuleIndex() const {
  return JsonParser::RuleNumber;
}

void JsonParser::NumberContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNumber(this);
}

void JsonParser::NumberContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNumber(this);
}

JsonParser::NumberContext* JsonParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 12, JsonParser::RuleNumber);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(74);
    _la = _input->LA(1);
    if (!(_la == JsonParser::NUMBER_DECIMAL

    || _la == JsonParser::NUMBER_FLOAT)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Null_typeContext ------------------------------------------------------------------

JsonParser::Null_typeContext::Null_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t JsonParser::Null_typeContext::getRuleIndex() const {
  return JsonParser::RuleNull_type;
}

void JsonParser::Null_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNull_type(this);
}

void JsonParser::Null_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNull_type(this);
}

JsonParser::Null_typeContext* JsonParser::null_type() {
  Null_typeContext *_localctx = _tracker.createInstance<Null_typeContext>(_ctx, getState());
  enterRule(_localctx, 14, JsonParser::RuleNull_type);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(76);
    match(JsonParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Bool_typeContext ------------------------------------------------------------------

JsonParser::Bool_typeContext::Bool_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t JsonParser::Bool_typeContext::getRuleIndex() const {
  return JsonParser::RuleBool_type;
}

void JsonParser::Bool_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBool_type(this);
}

void JsonParser::Bool_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<JsonListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBool_type(this);
}

JsonParser::Bool_typeContext* JsonParser::bool_type() {
  Bool_typeContext *_localctx = _tracker.createInstance<Bool_typeContext>(_ctx, getState());
  enterRule(_localctx, 16, JsonParser::RuleBool_type);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(78);
    _la = _input->LA(1);
    if (!(_la == JsonParser::T__7

    || _la == JsonParser::T__8)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> JsonParser::_decisionToDFA;
atn::PredictionContextCache JsonParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN JsonParser::_atn;
std::vector<uint16_t> JsonParser::_serializedATN;

std::vector<std::string> JsonParser::_ruleNames = {
  "json", "jsonObject", "jsonArray", "keyValuePair", "primitive", "string", 
  "number", "null_type", "bool_type"
};

std::vector<std::string> JsonParser::_literalNames = {
  "", "'{'", "','", "'}'", "'['", "']'", "':'", "'null'", "'true'", "'false'"
};

std::vector<std::string> JsonParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "SINGLE_STRING", "DOUBLE_STRING", 
  "NUMBER_DECIMAL", "NUMBER_FLOAT", "IDENTIFIER", "WS"
};

dfa::Vocabulary JsonParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> JsonParser::_tokenNames;

JsonParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x11, 0x53, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x3, 0x2, 0x7, 
    0x2, 0x16, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x19, 0xb, 0x2, 0x3, 0x2, 0x3, 
    0x2, 0x7, 0x2, 0x1d, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x20, 0xb, 0x2, 0x3, 
    0x2, 0x5, 0x2, 0x23, 0xa, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x7, 0x3, 0x29, 0xa, 0x3, 0xc, 0x3, 0xe, 0x3, 0x2c, 0xb, 0x3, 0x5, 0x3, 
    0x2e, 0xa, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x7, 0x4, 0x36, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x39, 0xb, 0x4, 0x5, 
    0x4, 0x3b, 0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 
    0x3, 0x5, 0x5, 0x5, 0x43, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x6, 0x5, 0x6, 0x49, 0xa, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 
    0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x2, 0x2, 0xb, 0x2, 
    0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x2, 0x5, 0x3, 0x2, 0xc, 0xd, 
    0x3, 0x2, 0xe, 0xf, 0x3, 0x2, 0xa, 0xb, 0x2, 0x54, 0x2, 0x22, 0x3, 0x2, 
    0x2, 0x2, 0x4, 0x24, 0x3, 0x2, 0x2, 0x2, 0x6, 0x31, 0x3, 0x2, 0x2, 0x2, 
    0x8, 0x3e, 0x3, 0x2, 0x2, 0x2, 0xa, 0x48, 0x3, 0x2, 0x2, 0x2, 0xc, 0x4a, 
    0x3, 0x2, 0x2, 0x2, 0xe, 0x4c, 0x3, 0x2, 0x2, 0x2, 0x10, 0x4e, 0x3, 
    0x2, 0x2, 0x2, 0x12, 0x50, 0x3, 0x2, 0x2, 0x2, 0x14, 0x16, 0x5, 0x4, 
    0x3, 0x2, 0x15, 0x14, 0x3, 0x2, 0x2, 0x2, 0x16, 0x19, 0x3, 0x2, 0x2, 
    0x2, 0x17, 0x15, 0x3, 0x2, 0x2, 0x2, 0x17, 0x18, 0x3, 0x2, 0x2, 0x2, 
    0x18, 0x1a, 0x3, 0x2, 0x2, 0x2, 0x19, 0x17, 0x3, 0x2, 0x2, 0x2, 0x1a, 
    0x23, 0x7, 0x2, 0x2, 0x3, 0x1b, 0x1d, 0x5, 0x6, 0x4, 0x2, 0x1c, 0x1b, 
    0x3, 0x2, 0x2, 0x2, 0x1d, 0x20, 0x3, 0x2, 0x2, 0x2, 0x1e, 0x1c, 0x3, 
    0x2, 0x2, 0x2, 0x1e, 0x1f, 0x3, 0x2, 0x2, 0x2, 0x1f, 0x21, 0x3, 0x2, 
    0x2, 0x2, 0x20, 0x1e, 0x3, 0x2, 0x2, 0x2, 0x21, 0x23, 0x7, 0x2, 0x2, 
    0x3, 0x22, 0x17, 0x3, 0x2, 0x2, 0x2, 0x22, 0x1e, 0x3, 0x2, 0x2, 0x2, 
    0x23, 0x3, 0x3, 0x2, 0x2, 0x2, 0x24, 0x2d, 0x7, 0x3, 0x2, 0x2, 0x25, 
    0x2a, 0x5, 0x8, 0x5, 0x2, 0x26, 0x27, 0x7, 0x4, 0x2, 0x2, 0x27, 0x29, 
    0x5, 0x8, 0x5, 0x2, 0x28, 0x26, 0x3, 0x2, 0x2, 0x2, 0x29, 0x2c, 0x3, 
    0x2, 0x2, 0x2, 0x2a, 0x28, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x2b, 0x3, 0x2, 
    0x2, 0x2, 0x2b, 0x2e, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x2a, 0x3, 0x2, 0x2, 
    0x2, 0x2d, 0x25, 0x3, 0x2, 0x2, 0x2, 0x2d, 0x2e, 0x3, 0x2, 0x2, 0x2, 
    0x2e, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x2f, 0x30, 0x7, 0x5, 0x2, 0x2, 0x30, 
    0x5, 0x3, 0x2, 0x2, 0x2, 0x31, 0x3a, 0x7, 0x6, 0x2, 0x2, 0x32, 0x37, 
    0x5, 0x4, 0x3, 0x2, 0x33, 0x34, 0x7, 0x4, 0x2, 0x2, 0x34, 0x36, 0x5, 
    0x4, 0x3, 0x2, 0x35, 0x33, 0x3, 0x2, 0x2, 0x2, 0x36, 0x39, 0x3, 0x2, 
    0x2, 0x2, 0x37, 0x35, 0x3, 0x2, 0x2, 0x2, 0x37, 0x38, 0x3, 0x2, 0x2, 
    0x2, 0x38, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x39, 0x37, 0x3, 0x2, 0x2, 0x2, 
    0x3a, 0x32, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x3b, 
    0x3c, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x3d, 0x7, 0x7, 0x2, 0x2, 0x3d, 0x7, 
    0x3, 0x2, 0x2, 0x2, 0x3e, 0x3f, 0x5, 0xc, 0x7, 0x2, 0x3f, 0x42, 0x7, 
    0x8, 0x2, 0x2, 0x40, 0x43, 0x5, 0xa, 0x6, 0x2, 0x41, 0x43, 0x5, 0x4, 
    0x3, 0x2, 0x42, 0x40, 0x3, 0x2, 0x2, 0x2, 0x42, 0x41, 0x3, 0x2, 0x2, 
    0x2, 0x43, 0x9, 0x3, 0x2, 0x2, 0x2, 0x44, 0x49, 0x5, 0xc, 0x7, 0x2, 
    0x45, 0x49, 0x5, 0x12, 0xa, 0x2, 0x46, 0x49, 0x5, 0xe, 0x8, 0x2, 0x47, 
    0x49, 0x5, 0x10, 0x9, 0x2, 0x48, 0x44, 0x3, 0x2, 0x2, 0x2, 0x48, 0x45, 
    0x3, 0x2, 0x2, 0x2, 0x48, 0x46, 0x3, 0x2, 0x2, 0x2, 0x48, 0x47, 0x3, 
    0x2, 0x2, 0x2, 0x49, 0xb, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x4b, 0x9, 0x2, 
    0x2, 0x2, 0x4b, 0xd, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x4d, 0x9, 0x3, 0x2, 
    0x2, 0x4d, 0xf, 0x3, 0x2, 0x2, 0x2, 0x4e, 0x4f, 0x7, 0x9, 0x2, 0x2, 
    0x4f, 0x11, 0x3, 0x2, 0x2, 0x2, 0x50, 0x51, 0x9, 0x4, 0x2, 0x2, 0x51, 
    0x13, 0x3, 0x2, 0x2, 0x2, 0xb, 0x17, 0x1e, 0x22, 0x2a, 0x2d, 0x37, 0x3a, 
    0x42, 0x48, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

JsonParser::Initializer JsonParser::_init;
